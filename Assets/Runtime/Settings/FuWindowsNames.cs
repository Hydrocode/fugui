namespace Fu.Core
{
    public enum FuWindowsNames
    {
        None = 0,
        WindowsDefinitionManager = 2,
        Tree = 3,
        Captures = 4,
        Inspector = 5,
        Metadata = 6,
        ToolBox = 7,
        MainCameraView = 8,
        FuguiSettings = 9,
        DockSpaceManager = 10,
        Modals = 11,
    }
}